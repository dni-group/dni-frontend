import Vue from "vue";
import Router from "vue-router";
import Main from "./components/Main.vue";
import SearchPage from "./views/SearchPage.vue";
import DataSetPage from "./views/DataSetPage.vue";
import SearchResults from "./views/SearchResults.vue";
import About from "./views/About.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "searchpage",
      component: SearchPage
    },
    {
      path: "/searchresults",
      name: "searchresults",
      component: SearchResults,
      // results/?tags=education&tags=employment
      props: (route) => ({
        tags: route.query.tags
      })
    },
    {
      path: "/dataset/:id",
      name: "dataset",
      component: DataSetPage,
      props: true
    },
    {
      path: "/about",
      name: "about",
      component: About
    }
  ]
});
