export const FILTER_SOURCES = 'filterSources';
export const FILTER_ORGANIZATIONS = 'filterOrganizations';
export const SET_LOGGED_IN_USER = 'SET_LOGGED_IN_USER';
