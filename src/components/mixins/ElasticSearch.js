import elasticsearch from 'elasticsearch';
import { networkInterfaces } from 'os';
import Vue from 'vue';
import moment from 'moment';


var client = new elasticsearch.Client({
  host: process.env.VUE_APP_DB,
  log: 'error'
});

export const elasticSearchMixin = {
  methods: {
    checkServer: function () {
      client.ping({
        // ping usually has a 3000ms timeout
        requestTimeout: 1000
      }, function (error) {
        if (error) {
          console.trace('elasticsearch cluster is down!');
        } else {
          console.log('All is well');
        }
      });
    },
    /**
     * Get stats of an index.
     * e.g number of documents in an index.
     * @param {*} index
     */
    getIndexStats: function getIndexStats (index) {
      return client.indices.stats({
        index: index
      });
    },
    /**
     * To build a query object that is compatible with
     * elastic search match criteria
     *
     * @param {*} field
     * @param {*} fieldDataArray
     * @returns  {*} queryArray of { match: { organization: 'Department of Education'}
     */
    buildQuery: function  (field, fieldDataArray) {
      var queryArray = [];
      if (fieldDataArray === undefined || fieldDataArray === '' || fieldDataArray.length == 0) {
        queryArray = [];
      }
      else if (typeof(fieldDataArray) == 'string') {
        queryArray.push({ match: { [field]: fieldDataArray } }); //incase it is only one search term.
      } else {
        fieldDataArray.forEach((fieldData) => {
          queryArray.push({ match: { [field]: fieldData } });
        });
      }
      return queryArray;
    },
    /**
     * Search query for
     *
     * @param {*} categories
     * @param {*} sources
     * @param {*} organizations
     */
    queryCategoryResults: function  (categories, sources, organizations) {
      var categoryQuery = this.buildQuery('category', categories);
      var sourceQuery = this.buildQuery('source', sources);
      var organizationQuery = this.buildQuery('organization', organizations);
      var currentDate = new Date();

      /**
       * Additionally to querying, this method also submits the query terms to a secondary index to assist the mostUsedTopics methods.
       */
      client.index({
        index: process.env.VUE_APP_DB_INDEX_2,
        type: 'query',
        body: {
          date: currentDate,
          query: categories
        }
      });

      return client.search({
          index: process.env.VUE_APP_DB_INDEX,
          //The current type is named 'docs', to be replaced accordingly when necessary
          type: 'docs',
          body: {
            query: {
              bool: {
                should: [
                  ...categoryQuery
                ],
                must: [
                  ...sourceQuery,
                  ...organizationQuery
                ]
              }
            }
          }
        });
    },
    uniqueListForField: function (field) {
      return client.search({
        index: process.env.VUE_APP_DB_INDEX,
        type: 'categories',
        body: {
          "aggs":{
            "unique_field": {
                "terms": {
                  "field": field
                }
            }
          }
        }
      });
    },
    mostUsedKeywords: function () {
      return client.search ({
        index: 'queries',
        type: 'query',
        body: {
          "query" : {
            "bool" : {
                "filter" : {
                    "range" : {
                        "date" : {
                            "gt" : "2018-01-01T00:00:00",
                            "lte" : "now/d"
                        }
                    }
                },
                "must" : {
                    "match_all" : {}
                }
            }
          },
          "aggs": {
              "keywords": {
                  "terms": {
                      "field": "query"
                  }
              }
          }
        }
      });
    }
  },
};