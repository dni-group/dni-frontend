import moment from 'moment';
import momenttz from 'moment-timezone';

/**
 * @param {*} currentTime
 * @param {*} time
 * @param {*} rangeInSeconds
 */
export function isWithinRangeBefore (currentTime, time, rangeInSeconds) {
  if (currentTime === undefined || currentTime === '') {
    return 'undefined';
  } else {
    var beforeTime = moment(time).subtract(rangeInSeconds, 'seconds');
    var startTime = moment(time);
    return moment(currentTime).isBetween(beforeTime, startTime);
  }
}

/**
 * @param {*} currentTime
 * @param {*} time
 * @param {*} rangeInSeconds
 */
export function isWithinRangeBeforeAndAfter (currentTime, time, rangeInSeconds) {
  if (currentTime === undefined || currentTime === '') {
    return 'undefined';
  } else {
    var beforeTime = moment(time).subtract(rangeInSeconds, 'seconds');
    var afterTime = moment(time).add(rangeInSeconds, 'seconds');
    return moment(currentTime).isBetween(beforeTime, afterTime);
  }
}

export function getDateTime (timezone) {
  if (timezone === undefined || timezone === '') {
    return 'undefined';
  } else {
    return moment().tz(timezone).format();
  }
}

export function convertTime (dateString) {
  if (dateString === undefined || dateString === '') {
    return 'undefined';
  } else {
    return momenttz(dateString).format('HH:mm');
  }
}

export function convertDay (dateString) {
  if (dateString === undefined || dateString === '') {
    return 'undefined';
  } else {
    return momenttz(dateString).format('Do');
  }
}

export function convertDayWithoutOrdinal (dateString) {
  if (dateString === undefined || dateString === '') {
    return 'undefined';
  } else {
    return momenttz(dateString).format('DD');
  }
}

export function convertMonth (dateString) {
  if (dateString === undefined || dateString === '') {
    return 'undefined';
  } else {
    return momenttz(dateString).format('MMM');
  }
}

export function convertDayMonth (dateString) {
  if (dateString === undefined || dateString === '') {
    return 'undefined';
  } else {
    return momenttz(dateString).format('Do MMM');
  }
}

export function convertDate (dateString) {
  if (dateString === undefined || dateString === '') {
    return 'undefined';
  } else {
    return momenttz(dateString).format('Do MMM YYYY');
  }
}

export function convertDateTime (dateString) {
  if (dateString === undefined || dateString === '') {
    return 'undefined';
  } else {
    return momenttz(dateString).format('HH:mm Do MMM YYYY');
  }
}

export function convertToTimeStamp (dateString) {
  if (dateString === undefined || dateString === '') {
    return 'undefined';
  } else {
    return momenttz(dateString).format('X'); // for seconds timestamp
  }
}


export function convertEpochToDate (epoch) {
  if (epoch === undefined || epoch === '') {
    return 'undefined';
  } else {
    var value = moment.unix(epoch).format('Do MMMM YYYY');
    return value;
  }
}

/**
 * Calculate the number of day from now until dateString
 * @param {*} dateString
 */
export function daysSince (dateString) {
  if (dateString === undefined || dateString === '') {
    return 'undefined';
  } else {
    var now = moment();
    var date = momenttz(dateString);
    return now.diff(date, 'days');
  }
}

/**
 * Checks if the current date is in the past
 * @param {*} dateString
 */
export function isPastDate (dateString) {
  if (dateString === undefined || dateString === '') {
    return 'undefined';
  } else {
    var now = moment();
    var date = momenttz(dateString);
    return date.isBefore(now);
  }
}

/**
 * Checks if the current date is in the future,
 * defaults to now date and time
 * @param {*} dateString
 * @param {*} now
 */
export function isFutureDate (dateString, now = moment()) {
  if (dateString === undefined || dateString === '') {
    return 'undefined';
  } else {
    var date = momenttz(dateString);
    return date.isAfter(now);
  }
}

/**
 * This is the date this week.
 * @param {*} dateString
 */
export function isInWeek (dateString, nowWeekNumber) {
  if (dateString === undefined || dateString === '') {
    return 'undefined';
  } else {
    var dateWeekNumber = momenttz(dateString).week();
    return nowWeekNumber === dateWeekNumber;
  }
}

/**
 * This is the date this week.
 * @param {*} dateString
 */
export function isThisWeek (dateString) {
  var nowWeekNumber = moment().week();
  return isInWeek(dateString, nowWeekNumber);
}

/**
* used to create a array of dates from a list of objects. Used for date drops downs.
*/
export function dateList (objects) {
  var dateList = [];
  objects.filter((event) => {
    if (event.createdAt && dateList.indexOf(convertDate(event.createdAt.date)) === -1) {
      dateList.push(convertDate(event.createdAt.date));
    }
  });
  return dateList;
}

export function filter (objects, prop) {
  var resultList = [];
  objects.filter((event) => {
    if (resultList.indexOf(event[prop]) === -1) {
      resultList.push(event[prop]);
    }
  });
  return resultList;
}

/**
*  returns a list of results that match 1 layer or 2 layer props that equal the selected variable.
*/
export function selectedFiltered (result, selected, prop1, prop2) {
  if (selected !== '') {
    result = result.filter((element) => {
      if (prop2) {
        return element[prop1][prop2] === selected;
      } else {
        return element[prop1] === selected;
      }
    });
  }
  return result;
}
