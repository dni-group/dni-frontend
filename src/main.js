import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import BootstrapVue from "bootstrap-vue";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import VueWait from "vue-wait";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faBars, faSearch, faChevronRight } from "@fortawesome/free-solid-svg-icons";
import {
  faTwitter,
  faFacebook,
  faStackOverflow,
  faGithub
} from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import VoerroTagsInput from '@voerro/vue-tagsinput';
import VueEvents from 'vue-events';
import elasticsearch from 'elasticsearch';
import moment from 'moment';

library.add(faBars, faSearch, faChevronRight, faTwitter, faFacebook, faStackOverflow, faGithub);
Vue.component("font-awesome-icon", FontAwesomeIcon);

// import { library } from '@fortawesome/fontawesome-svg-core';
// import { faUser, faSpinner } from '@fortawesome/free-solid-svg-icons';
// import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

// library.add(faUser);
// library.add(faSpinner);

// Vue.component('font-awesome-icon', FontAwesomeIcon);

Vue.use(VueWait);
Vue.use(VueEvents);
Vue.use(BootstrapVue);
Vue.component('tags-input', VoerroTagsInput);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App),
  wait: new VueWait()
}).$mount("#app");
