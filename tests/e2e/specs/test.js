// https://docs.cypress.io/api/introduction/api.html

describe("Search Page", () => {
  it("Check page exists", () => {
    cy.visit("/");
    cy.get('#app').should('be.visible');
  });
  it("Check the Navigation exists  ", () => {
    cy.visit("/");
    cy.get('#navigation').should('be.visible');
  });
  it("Check the search button exists  ", () => {
    cy.visit("/");
    cy.contains("#search", "Search Data Times");
  });
});