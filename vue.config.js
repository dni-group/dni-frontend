const path = require('path')

module.exports = {
  baseUrl: process.env.NODE_ENV === 'production' ? '/' : '/', // change to S3 bucket for production
  configureWebpack: config => {
     if (process.env.NODE_ENV === 'production') {
      // mutate config for production...
      plugins: []
    } else {
      // mutate for development...
      plugins: []
    }
  },
  chainWebpack: (config) => {
      /* var test = path.resolve(__dirname, 'test');
      var src = path.resolve(__dirname, 'src');
      var nodes = path.resolve(__dirname, 'node_modules');
      config.module
        .rule('istanbul')
          .test(/\.(js|vue)$/)
          .enforce('post')
          .include.add(src)
          .end()
          .exclude.add(nodes)
          .end()
          .use('istanbul-instrumenter-loader')
            .loader('istanbul-instrumenter-loader')
            .options({ debug: true, esModules: true }); */
  },
  devServer: {
    public: 'http://localhost:8080',
    headers: {
      'Access-Control-Allow-Origin': '*'
    },
    open: 'true', // opens the browser and the server is started,
    proxy: {
      '/dni': {
         'target': 'http://localhost:9200',
      	 changeOrigin: true
      },
      '/_search': {
        'target': 'http://localhost:9200',
        changeOrigin: true
     }
    }
  }
}

